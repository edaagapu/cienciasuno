function limpiar(){
    var contenedor = document.getElementById("contenedor");
    if (contenedor){
		contenedor.parentNode.removeChild(contenedor);
	}
}

function crearMatriz(){
        limpiar();
    var tam = parseInt(document.getElementById("tamanoMatriz").value);
    var principal =  document.getElementById("matriz");   
    var contenedor = document.createElement("div");
    contenedor.id = "contenedor";
    principal.appendChild(contenedor);
    var elementos = new Array(tam);
    for (i=0;i<tam;i++){
        var fila = document.createElement("div");
        contenedor.appendChild(fila);        
        elementos[i] = new Array(tam);
        for (j=0;j<tam;j++){
            elementos[i][j] = document.createElement("input");
            elementos[i][j].type = "text";
            elementos[i][j].id = "matriz"+i.toString()+j.toString();
            elementos[i][j].size = "1";
            elementos[i][j].value = "0";
            fila.appendChild(elementos[i][j]);
        }
    }
}

function determinante(){
    var N = parseInt(document.getElementById("tamanoMatriz").value);
    var inter=0;
    var contador = 0;
    var formula = (2*(Math.pow(N+1,3)) - (9*Math.pow(N+1,2)) + (13*(N+1)) - 6)/6;
    var m = new Array(N);
    for(i=0;i<N;i++){
        m[i] = new Array(N);
        for(j=0;j<N;j++){
            m[i][j] = parseInt(document.getElementById("matriz"+i.toString()+j.toString()).value);
        }    
    }
    for (i = 0 ; i<N-1 ; i++){
        for (j = i+1 ; j<N ; j++){
            for (k=i+1 ; k<N ; k++){
                var tempo = m[k][j] - (m[k][i]*m[i][j])/m[i][i];
                while(tempo==0){                  
                    intercambiar(m,k,k+1,N);
                    inter++;
                    tempo = m[k][j] - (m[k][i]*m[i][j])/m[i][i];                
                }
                m[k][j] = tempo;
                contador++;
            }
        }
    }
    tr=1;
    for (i=0; i<N; i++){
        tr=tr*m[i][i];
        contador++;
    }
    tr=tr*Math.pow(-1,inter);
    formula = formula + N;
    formule = document.getElementById("formula");
    formule.innerHTML="Formula: "+formula.toString();
    conteo = document.getElementById("conteo");
    conteo.innerHTML="Contador: "+contador.toString();
    resultado = document.getElementById("resultado");
    resultado.innerHTML="El resultado es: "+tr.toString();
    console.log(tr);
}

function intercambiar(matriz,posO,posD,tamano){
    for (l=posO;l<tamano;l++){
        tmp = matriz[posO][l];
        matriz[posO][l] = matriz[posD][l];
        matriz[posD][l] = tmp; 
    }
}