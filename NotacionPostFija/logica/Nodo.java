package logica;

public class Nodo {
	private String valor;
	private Nodo siguiente;

	public Nodo() {
		siguiente = null;
		valor = "";
	}

	public Nodo(String valor) {
		this.valor = valor;
		siguiente = null;
	}

	public Nodo getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(Nodo siguiente) {
		this.siguiente = siguiente;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
