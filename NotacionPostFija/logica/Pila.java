package logica;

public class Pila {
	private Nodo cabeza = null;
	private Nodo centinela = null;

	public Pila() {
		cabeza = new Nodo();
	}

	public String desapilar() {
		if (!estaVacia()) {
			centinela = cabeza;
			Nodo tempo = cabeza.getSiguiente();
			while (tempo.getSiguiente() != null) {
				centinela = tempo;
				tempo = tempo.getSiguiente();
			}
			String resultado = tempo.getValor();
			centinela.setSiguiente(null);
			return resultado;
		}
		return "";
	}

	public boolean estaVacia() {
		return cabeza.getSiguiente() == null;
	}

	public void apilar(String valor) {
		centinela = cabeza;
		while (centinela.getSiguiente() != null) {
			centinela = centinela.getSiguiente();
		}
		centinela.setSiguiente(new Nodo(valor));
	}
}
