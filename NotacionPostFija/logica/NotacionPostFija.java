package logica;

import java.util.ArrayList;

public class NotacionPostFija {
	private Pila pila = null;
	private MatrizPrioridad matriz = null;

	public NotacionPostFija() {
		inicializar();
		matriz = new MatrizPrioridad();
	}

	public void inicializar() {
		pila = new Pila();
	}

	public String[] notacion(String operacion) throws Exception {
		inicializar();
		ArrayList<String> lista = new ArrayList<String>();
		String[] inicial = separacion(operacion);

		for (int i = 0; i < inicial.length; i++) {
			if (inicial[i].substring(0, 1).matches("[0-9]")) {
				lista.add(inicial[i]);
			} else {
				if (!pila.estaVacia()) {
					String primero = pila.desapilar();
					String segundo = inicial[i];
					if (!segundo.equals(")")) {
						if (matriz.getValor(primero, segundo)) {
							lista.add(primero);
							pila.apilar(segundo);
						} else {
							pila.apilar(primero);
							pila.apilar(segundo);
						}
					} else {
						while (!primero.equals("(")) {
							lista.add(primero);
							primero = pila.desapilar();
						}
					}
				} else {
					pila.apilar(inicial[i]);
				}
			}
		}
		while (!pila.estaVacia()) {
			lista.add(pila.desapilar());
		}

		String[] resultado = new String[lista.size()];
		for (int j = 0; j < resultado.length; j++) {
			resultado[j] = lista.get(j);
		}
		return resultado;
	}

	public double operar(String[] postFija) {
		inicializar();
		for (int i = 0; i < postFija.length; i++) {
			if (postFija[i].substring(0, 1).matches("[0-9]")) {
				pila.apilar(postFija[i]);
			} else {
				String segundo = pila.desapilar();
				String primero = pila.desapilar();
				String resultado = Double.toString(operacion(primero, segundo, postFija[i]));
				pila.apilar(resultado);
			}
		}
		return Double.parseDouble(pila.desapilar());
	}

	private double operacion(String primero, String segundo, String operacion) {
		double numU = Double.parseDouble(primero);
		double numD = Double.parseDouble(segundo);
		switch (operacion) {
		case "+":
			return numU + numD;
		case "-":
			return numU - numD;
		case "*":
			return numU * numD;
		case "/":
			return numU / numD;
		default:
			return Math.pow(numU, numD);
		}

	}

	private String[] separacion(String operaciones) {
		String[] flag = operaciones.split("");
		String fl = "";
		ArrayList<String> lista = new ArrayList<String>();
		for (int i = 0; i < flag.length; i++) {
			lista.add(flag[i]);
		}
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).substring(0, 1).matches("[0-9]")) {
				if (i < lista.size() - 1) {
					int j = i + 1;
					while (lista.get(j).substring(0, 1).matches("[0-9]")) {
						fl = lista.get(i) + lista.get(j);
						lista.remove(j);
						lista.set(i, fl);
					}
					fl = "";
				}
			}
		}
		flag = new String[lista.size()];
		for (int i = 0; i < lista.size(); i++) {
			flag[i] = lista.get(i);
		}
		return flag;
	}

}
