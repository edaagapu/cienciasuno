package logica;

public class MatrizPrioridad {
	public boolean[][] matriz = { { true, true, true, true, true, false, true },
			{ false, true, true, true, true, false, true }, { false, true, true, true, true, false, true },
			{ false, false, false, true, true, false, true }, { false, false, false, true, true, false, true },
			{ false, false, false, false, false, false, true } };
	public String[] hashing = { "^", "/", "*", "+", "-", "(", ")" };

	public int hash(String symbol) {
		for (int i = 0; i < hashing.length; i++) {
			if (symbol.equals(hashing[i])) {
				return i;
			}
		}
		return -1;
	}

	public MatrizPrioridad() {
	}

	public boolean getValor(String symbolU, String symbolD) throws Exception {
		int primero = hash(symbolU);
		int segundo = hash(symbolD);
		if (primero >= 0 && segundo >= 0) {
			return matriz[primero][segundo];
		} else {
			throw new Exception("Valor inválido");
		}
	}
}