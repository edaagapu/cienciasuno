package ventana;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class VPrincipal extends JFrame {

	private JPanel contentPane;
	public JTextField txtOperaciones;
	public JButton btnCalcular;
	public JButton btnNotacion;
	public JLabel lblNotacion;

	public VPrincipal() {
		IPrincipal controlador = new IPrincipal(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 270);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel pnlPrincipal = new JPanel();
		pnlPrincipal.setBorder(new LineBorder(Color.BLACK));
		pnlPrincipal.setBackground(Color.WHITE);
		pnlPrincipal.setBounds(10, 10, 280, 225);
		pnlPrincipal.setLayout(null);
		contentPane.add(pnlPrincipal);

		lblNotacion = new JLabel("");
		lblNotacion.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNotacion.setBounds(10, 10, 260, 60);
		pnlPrincipal.add(lblNotacion);

		txtOperaciones = new JTextField();
		txtOperaciones.setBackground(new Color(255, 255, 224));
		txtOperaciones.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtOperaciones.setBounds(10, 80, 260, 30);
		pnlPrincipal.add(txtOperaciones);
		txtOperaciones.setColumns(10);

		btnNotacion = new JButton("Notacion");
		btnNotacion.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnNotacion.setBackground(Color.WHITE);
		btnNotacion.setBounds(20, 120, 90, 30);
		btnNotacion.addActionListener(controlador);
		pnlPrincipal.add(btnNotacion);

		btnCalcular = new JButton("Calcular");
		btnCalcular.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnCalcular.setBackground(Color.WHITE);
		btnCalcular.setBounds(120, 120, 90, 30);
		btnCalcular.setVisible(false);
		btnCalcular.addActionListener(controlador);
		pnlPrincipal.add(btnCalcular);

	}
}
