package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import logica.NotacionPostFija;

public class IPrincipal implements ActionListener {
	private VPrincipal ventana;
	private String[] notacion;
	private NotacionPostFija logica;

	public IPrincipal(VPrincipal ventana) {
		this.ventana = ventana;
		notacion = null;
		logica = new NotacionPostFija();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(ventana.btnCalcular)) {
			ventana.txtOperaciones.setText("");
			ventana.btnCalcular.setVisible(false);
			JOptionPane.showMessageDialog(null, Double.toString(logica.operar(notacion)));
			ventana.lblNotacion.setText("");
		} else if (boton.equals(ventana.btnNotacion)) {
			ventana.btnCalcular.setVisible(true);
			try {
				notacion = logica.notacion(ventana.txtOperaciones.getText());
				ventana.lblNotacion.setText(convertir(notacion));
			} catch (Exception exc) {
				exc.printStackTrace();
			}
		}
	}

	private String convertir(String[] arreglo) {
		String resultado = "";
		for (int i = 0; i < arreglo.length; i++) {
			resultado = resultado + arreglo[i];
			if (i < arreglo.length - 1) {
				resultado = resultado + " ";
			}
		}
		return resultado;
	}

}
