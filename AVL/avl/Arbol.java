package avl;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Arbol {
	private NodoArbol raiz;
	private Arbol izquierda, derecha;

	public Arbol(String valor) {
		setRaiz(new NodoArbol(valor));
		setIzquierda(null);
		setDerecha(null);
	}

	public static Arbol reconstruir(String[] claves, int[] arr) {
		Arbol resultado = new Arbol(claves[0]);
		for (int i = 1; i < claves.length; i++) {
			resultado.agregarElemento(resultado, claves[i]);
			resultado = resultado.balancearArbol(resultado, arr);
		}

		return resultado;
	}

	public NodoArbol getRaiz() {
		return raiz;
	}

	public void setRaiz(NodoArbol raiz) {
		this.raiz = raiz;
	}

	public Arbol getIzquierda() {
		return izquierda;
	}

	public void setIzquierda(Arbol izquierda) {
		this.izquierda = izquierda;
	}

	public Arbol getDerecha() {
		return derecha;
	}

	public Arbol setDerecha(Arbol derecha) {
		this.derecha = derecha;
		return derecha;
	}

	public ArrayList<String> preOrden() {
		ArrayList<String> resultado = new ArrayList<String>();
		resultado.add(getRaiz().getValor());
		if (getIzquierda() != null) {
			resultado.addAll(getIzquierda().preOrden());
		}
		if (getDerecha() != null) {
			resultado.addAll(getDerecha().preOrden());
		}
		return resultado;
	}

	public ArrayList<String> inOrden() {
		ArrayList<String> resultado = new ArrayList<String>();
		if (getIzquierda() != null) {
			resultado.addAll(getIzquierda().inOrden());
		}
		resultado.add(getRaiz().getValor());
		if (getDerecha() != null) {
			resultado.addAll(getDerecha().inOrden());
		}
		return resultado;
	}

	public ArrayList<String> postOrden() {
		ArrayList<String> resultado = new ArrayList<String>();
		if (getIzquierda() != null) {
			resultado.addAll(getIzquierda().postOrden());
		}
		if (getDerecha() != null) {
			resultado.addAll(getDerecha().postOrden());
		}
		resultado.add(getRaiz().getValor());
		return resultado;
	}

	public int cantNiveles() {
		Arbol tempo = this;
		int recorrido = 1, recIzq = 0, recDer = 0;
		if (tempo.getIzquierda() != null || tempo.getDerecha() != null) {
			if (tempo.getIzquierda() != null) {
				recIzq = tempo.getIzquierda().cantNiveles();
			}
			if (tempo.getDerecha() != null) {
				recDer = tempo.getDerecha().cantNiveles();
			}
			if (recDer < recIzq) {
				return recorrido + recIzq;
			} else {
				return recorrido + recDer;
			}
		} else {
			return 0;
		}
	}

	public int getBalance() {
		Arbol tempo = this;
		int balIzq = 0;
		int balDer = 0;

		if (tempo.getIzquierda() != null || tempo.getDerecha() != null) {
			if (tempo.getIzquierda() != null) {
				balIzq = tempo.getIzquierda().cantNiveles() + 1;
			}
			if (tempo.getDerecha() != null) {
				balDer = tempo.getDerecha().cantNiveles() + 1;
			}
			return balDer - balIzq;
		} else {
			return 0;
		}
	}

	public boolean agregarElemento(Arbol arbol, String clave) {
		if (Integer.parseInt(arbol.getRaiz().getValor()) < Integer.parseInt(clave)) {
			if (arbol.getDerecha() != null) {
				return agregarElemento(arbol.getDerecha(), clave);
			} else {
				arbol.setDerecha(new Arbol(clave));
				return true;
			}
		} else if (Integer.parseInt(arbol.getRaiz().getValor()) > Integer.parseInt(clave)) {
			if (arbol.getIzquierda() != null) {
				return agregarElemento(arbol.getIzquierda(), clave);
			} else {
				arbol.setIzquierda(new Arbol(clave));
				return true;
			}
		} else {
			return false;
		}
	}

	public Arbol balancearArbol(Arbol arbol, int[] arr) {
		Arbol resultado = arbol;
		Arbol centIzq = null;
		centIzq = arbol.getIzquierda();
		Arbol centDer = null;
		centDer = arbol.getDerecha();
		int balDer = 0, balIzq = 0;
		if (centDer != null) {
			arr[4]++;
			centDer = balancearArbol(centDer, arr);
			balDer = centDer.getBalance();
		}
		if (centIzq != null) {
			arr[4]++;
			centIzq = balancearArbol(centIzq, arr);
			balIzq = centIzq.getBalance();
		}
		resultado.setDerecha(null);
		resultado.setIzquierda(null);
		resultado.setDerecha(centDer);
		resultado.setIzquierda(centIzq);

		if ((resultado.getBalance() * balDer > 1) || (resultado.getBalance() * balIzq > 1)) {
			arr[4]++;
			if (resultado.getBalance() > 1) {
				arr[4]++;
				arr[0]++;
				resultado = rotarIzquierda(resultado, arr[4]);
			} else if (resultado.getBalance() < -1) {
				arr[1]++;
				resultado = rotarDerecha(resultado, arr[4]);
			}
		} else if ((resultado.getBalance() * balDer < -1) || (resultado.getBalance() * balIzq < -1)) {
			if (resultado.getBalance() > 1) {
				arr[4]++;
				arr[2]++;
				resultado.setDerecha(rotarDerecha(centDer, arr[4]));
				resultado = rotarIzquierda(resultado, arr[4]);
			} else if (resultado.getBalance() < -1) {
				arr[3]++;
				resultado.setIzquierda(rotarIzquierda(centIzq, arr[4]));
				resultado = rotarDerecha(resultado, arr[4]);
			}
		} else {
			if (resultado.getBalance() > 1) {
				arr[4]++;
				arr[0]++;
				resultado = rotarIzquierda(resultado, arr[4]);
			} else if (resultado.getBalance() < -1) {
				arr[1]++;
				resultado = rotarDerecha(resultado, arr[4]);
			}
		}
		return resultado;
	}

	private Arbol rotarDerecha(Arbol arbol, int num) {
		Arbol resultado = arbol.getIzquierda();
		arbol.setIzquierda(null);
		if (resultado.getDerecha() != null) {
			num++;
			arbol.setIzquierda(resultado.getDerecha());
		}
		resultado.setDerecha(null);
		resultado.setDerecha(arbol);
		return resultado;
	}

	private Arbol rotarIzquierda(Arbol arbol, int num) {
		Arbol resultado = arbol.getDerecha();
		arbol.setDerecha(null);
		if (resultado.getIzquierda() != null) {
			num++;
			arbol.setDerecha(resultado.getIzquierda());
		}
		resultado.setIzquierda(null);
		resultado.setIzquierda(arbol);
		return resultado;
	}

	public Arbol buscarElemento(Arbol arbol, String clave) {
		if (Integer.parseInt(arbol.getRaiz().getValor()) < Integer.parseInt(clave)) {
			if (arbol.getDerecha() != null) {
				return buscarElemento(arbol.getDerecha(), clave);
			} else {
				return null;
			}
		} else if (Integer.parseInt(arbol.getRaiz().getValor()) > Integer.parseInt(clave)) {
			if (arbol.getIzquierda() != null) {
				return buscarElemento(arbol.getIzquierda(), clave);
			} else {
				return null;
			}
		}
		return arbol;
	}

	private Arbol buscarPadre(Arbol arbol, String clave) {
		if (Integer.parseInt(arbol.getRaiz().getValor()) < Integer.parseInt(clave)) {
			if (arbol.getDerecha() != null) {
				if (arbol.getDerecha().getRaiz().getValor().equals(clave)) {
					return arbol;
				}
				return buscarPadre(arbol.getDerecha(), clave);
			} else {
				return null;
			}
		} else if (Integer.parseInt(arbol.getRaiz().getValor()) > Integer.parseInt(clave)) {
			if (arbol.getIzquierda() != null) {
				if (arbol.getIzquierda().getRaiz().getValor().equals(clave)) {
					return arbol;
				}
				return buscarPadre(arbol.getIzquierda(), clave);
			} else {
				return null;
			}
		}
		return null;
	}

	public boolean removerElemento(Arbol arbol, String clave) {
		Arbol eliminar = null;
		Arbol padre = null;
		Arbol centDer = null, centIzq = null;
		eliminar = buscarElemento(arbol, clave);
		padre = buscarPadre(arbol, clave);
		if (eliminar != null) {
			centDer = eliminar.getDerecha();
			centIzq = eliminar.getIzquierda();
			if (centDer != null && centIzq != null) {
				if (padre != null) {

				} else {

				}
				Arbol reemplazar = null;
				reemplazar = centDer;
				while (reemplazar.getIzquierda() != null) {
					reemplazar = reemplazar.getIzquierda();
				}
				String flag = reemplazar.getRaiz().getValor();
				removerElemento(arbol, flag);
				eliminar.setRaiz(new NodoArbol(flag));
			} else if (centDer != null ^ centIzq != null) {
				if (padre != null) {
					if (Integer.parseInt(padre.getRaiz().getValor()) > Integer.parseInt(clave)) {
						if (centDer != null) {
							padre.setIzquierda(centDer);
						} else {
							padre.setIzquierda(centIzq);
						}
					} else {
						if (centDer != null) {
							padre.setDerecha(centDer);
						} else {
							padre.setDerecha(centIzq);
						}
					}
				} else {
					if (centDer != null) {
						eliminar.setRaiz(centDer.getRaiz());
						eliminar.setDerecha(centDer.getDerecha());
						eliminar.setIzquierda(centDer.getIzquierda());
					} else {
						eliminar.setRaiz(centIzq.getRaiz());
						eliminar.setDerecha(centIzq.getDerecha());
						eliminar.setIzquierda(centIzq.getIzquierda());
					}
				}
			} else {
				if (padre != null) {
					if (Integer.parseInt(padre.getRaiz().getValor()) > Integer.parseInt(clave)) {
						padre.setIzquierda(null);
					} else {
						padre.setDerecha(null);
					}
				} else {
					JOptionPane.showMessageDialog(null, "No es posible eliminar la raíz");
					return false;
				}
			}
			return true;
		} else {
			return false;
		}

	}
}
