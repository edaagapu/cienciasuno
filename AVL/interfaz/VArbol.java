package interfaz;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import avl.Arbol;
import rojinegro.ArbolRN;

public class VArbol extends JFrame {

	public PArbol pnlArbol;
	private JPanel contentPane;
	private int parametro = 1;
	private ArrayList<NodoGrafico[]> nodos = null;
	private String[] textoBotones = { "Agregar"};
	//private String[] textoLabel = { "RI: ", "RD: ", "RDI: ", "RDD: " };
	public JButton[] btnArbol = new JButton[textoBotones.length];
	//public JLabel[] lblRotar = new JLabel[textoLabel.length];
	public Arbol arbol;
	public ArbolRN rojinegro;
	private JScrollPane scrollPane;

	public VArbol(ArbolRN arbol, int[] arr) {
		IArbolBotones ctr = new IArbolBotones(this, arr);
		this.rojinegro = arbol;
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);

		setContentPane(contentPane);
		contentPane.setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 750, 450);
		contentPane.add(scrollPane);

		panelArbolRN();
		adaptarArbolRN(0, 0);
		removerNodosRN();
		for (int i = 0; i < btnArbol.length; i++) {
			btnArbol[i] = new JButton(textoBotones[i]);
			btnArbol[i].setBounds(790, 20 + (50 * i), 90, 30);
			btnArbol[i].setBorder(new LineBorder(Color.BLACK));
			btnArbol[i].setBackground(Color.WHITE);
			btnArbol[i].addActionListener(ctr);
			contentPane.add(btnArbol[i]);
		}
//		for (int i = 0; i < arr.length-1; i++) {
//			lblRotar[i] = new JLabel(textoLabel[i] + Integer.toString(arr[i]));
//			lblRotar[i].setBounds(790, 20 + (50 * (textoBotones.length + i)), 90, 30);
//			lblRotar[i].setBackground(Color.WHITE);
//			contentPane.add(lblRotar[i]);
//		}
		scrollPane.setViewportView(pnlArbol);
		setBounds(300, 60, 900, 500);
	}

	
	public VArbol(Arbol arbol, int[] arr) {
		IArbolBotones ctr = new IArbolBotones(this, arr);
		this.arbol = arbol;
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);

		setContentPane(contentPane);
		contentPane.setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 750, 450);
		contentPane.add(scrollPane);

		panelArbol();
		adaptarArbol(0, 0);
		removerNodos();
		for (int i = 0; i < btnArbol.length; i++) {
			btnArbol[i] = new JButton(textoBotones[i]);
			btnArbol[i].setBounds(790, 20 + (50 * i), 90, 30);
			btnArbol[i].setBorder(new LineBorder(Color.BLACK));
			btnArbol[i].setBackground(Color.WHITE);
			btnArbol[i].addActionListener(ctr);
			contentPane.add(btnArbol[i]);
		}
//		for (int i = 0; i < arr.length-1; i++) {
//			lblRotar[i] = new JLabel(textoLabel[i] + Integer.toString(arr[i]));
//			lblRotar[i].setBounds(790, 20 + (50 * (textoBotones.length + i)), 90, 30);
//			lblRotar[i].setBackground(Color.WHITE);
//			contentPane.add(lblRotar[i]);
//		}
		scrollPane.setViewportView(pnlArbol);
		setBounds(300, 60, 900, 500);
	}

//	public void setTextLabel(int i, int valor) {
//		lblRotar[i].setText(textoLabel[i]+Integer.toString(valor));
//	}
	
	public void removerNodos() {
		for (int i = 0; i < nodos.size(); i++) {
			for (int j = 0; j < nodos.get(i).length; j++) {
				if (nodos.get(i)[j].getText().equals("")) {
					pnlArbol.remove(nodos.get(i)[j]);
					nodos.get(i)[j] = null;
				}
			}
		}
		pnlArbol.setNodos(nodos);
		scrollPane.setViewportView(pnlArbol);
	}

	public void panelArbol() {
		parametro = arbol.cantNiveles();
		nodos = new ArrayList<NodoGrafico[]>();
		pnlArbol = new PArbol((int) (Math.pow(2, parametro) * 70), ((parametro + 1) * 50));
		pnlArbol.setPreferredSize(pnlArbol.getSize());
		for (int i = 0; i <= parametro + 1; i++) {
			nodos.add(new NodoGrafico[(int) (Math.pow(2, i))]);
			for (int j = 0; j < nodos.get(i).length; j++) {
				nodos.get(i)[j] = new NodoGrafico("",
						(int) (pnlArbol.getWidth() / Math.pow(2, i + 1)) * ((2 * j) + 1) - 25, 10 + 50 * i);
				pnlArbol.add(nodos.get(i)[j]);
			}
		}
		for (int i = 0; i < nodos.get(nodos.size() - 1).length; i++) {
			pnlArbol.remove(nodos.get(nodos.size() - 1)[i]);
		}
		nodos.remove(nodos.size() - 1);
	}

	public void adaptarArbol(int i, int j) {
		Color color = Color.YELLOW;
		if (arbol != null) {
			nodos.get(i)[j].setText(arbol.getRaiz().getValor());
			switch (arbol.getBalance()) {
			case 0:
				color = Color.BLACK;
				break;
			case -1:
				color = Color.RED;
				break;
			case 1:
				color = Color.BLUE;
				break;
			}
			nodos.get(i)[j].setBorder(new LineBorder(color));
			nodos.get(i)[j].setForeground(color);

			int tempoU = i, tempoD = j;
			Arbol centi = arbol;
			while (centi.getIzquierda() != null) {
				centi = centi.getIzquierda();
				i++;
				j = 2 * j;
				adaptarArbol(centi, i, j);
			}
			centi = arbol;
			i = tempoU;
			j = tempoD;
			while (centi.getDerecha() != null) {
				centi = centi.getDerecha();
				i++;
				j = 2 * j + 1;
				adaptarArbol(centi, i, j);
			}
		}
	}

	private void adaptarArbol(Arbol arbol, int i, int j) {
		Color color = Color.DARK_GRAY;
		if (arbol != null) {
			nodos.get(i)[j].setText(arbol.getRaiz().getValor());
			switch (arbol.getBalance()) {
			case 0:
				color = Color.BLACK;
				break;
			case -1:
				color = Color.RED;
				break;
			case 1:
				color = Color.BLUE;
				break;
			}
			nodos.get(i)[j].setBorder(new LineBorder(color));
			nodos.get(i)[j].setForeground(color);

			int tempoU = i, tempoD = j;
			Arbol centi = arbol;
			while (centi.getIzquierda() != null) {
				centi = centi.getIzquierda();
				i++;
				j = 2 * j;
				adaptarArbol(centi, i, j);
			}
			centi = arbol;
			i = tempoU;
			j = tempoD;
			while (centi.getDerecha() != null) {
				centi = centi.getDerecha();
				i++;
				j = 2 * j + 1;
				adaptarArbol(centi, i, j);
			}
		}
	}
	
	
	// Métodos con Rojinegros
	

	public void removerNodosRN() {
		for (int i = 0; i < nodos.size(); i++) {
			for (int j = 0; j < nodos.get(i).length; j++) {
				if (nodos.get(i)[j].getText().equals("")) {
					pnlArbol.remove(nodos.get(i)[j]);
					nodos.get(i)[j] = null;
				}
			}
		}
		pnlArbol.setNodos(nodos);
		scrollPane.setViewportView(pnlArbol);
	}

	public void panelArbolRN() {
		parametro = rojinegro.cantNiveles();
		nodos = new ArrayList<NodoGrafico[]>();
		pnlArbol = new PArbol((int) (Math.pow(2, parametro) * 70), ((parametro + 1) * 50));
		pnlArbol.setPreferredSize(pnlArbol.getSize());
		for (int i = 0; i <= parametro + 1; i++) {
			nodos.add(new NodoGrafico[(int) (Math.pow(2, i))]);
			for (int j = 0; j < nodos.get(i).length; j++) {
				nodos.get(i)[j] = new NodoGrafico("",
						(int) (pnlArbol.getWidth() / Math.pow(2, i + 1)) * ((2 * j) + 1) - 25, 10 + 50 * i);
				pnlArbol.add(nodos.get(i)[j]);
			}
		}
		for (int i = 0; i < nodos.get(nodos.size() - 1).length; i++) {
			pnlArbol.remove(nodos.get(nodos.size() - 1)[i]);
		}
		nodos.remove(nodos.size() - 1);
	}

	public void adaptarArbolRN(int i, int j) {
		Color color = Color.YELLOW;
		if (rojinegro != null) {
			nodos.get(i)[j].setText(rojinegro.getValor());
			if (ArbolRN.isColor(rojinegro)) {
				color = Color.ORANGE;
			} else {
				color = Color.DARK_GRAY;
			}
			
			nodos.get(i)[j].setBorder(new LineBorder(color));
			nodos.get(i)[j].setForeground(color);

			int tempoU = i, tempoD = j;
			ArbolRN centi = rojinegro;
			while (centi.getIzquierda() != null) {
				centi = centi.getIzquierda();
				i++;
				j = 2 * j;
				adaptarArbolRN(centi, i, j);
			}
			centi = rojinegro;
			i = tempoU;
			j = tempoD;
			while (centi.getDerecha() != null) {
				centi = centi.getDerecha();
				i++;
				j = 2 * j + 1;
				adaptarArbolRN(centi, i, j);
			}
		}
	}

	private void adaptarArbolRN(ArbolRN arbol, int i, int j) {
		Color color = Color.DARK_GRAY;
		if (arbol != null) {
			nodos.get(i)[j].setText(arbol.getValor());
			if (ArbolRN.isColor(arbol)) {
				color = Color.RED;
			} else {
				color = Color.BLACK;
			}
			nodos.get(i)[j].setBorder(new LineBorder(color));
			nodos.get(i)[j].setForeground(color);

			int tempoU = i, tempoD = j;
			ArbolRN centi = arbol;
			while (centi.getIzquierda() != null) {
				centi = centi.getIzquierda();
				i++;
				j = 2 * j;
				adaptarArbolRN(centi, i, j);
			}
			centi = arbol;
			i = tempoU;
			j = tempoD;
			while (centi.getDerecha() != null) {
				centi = centi.getDerecha();
				i++;
				j = 2 * j + 1;
				adaptarArbolRN(centi, i, j);
			}
		}
	}
}
