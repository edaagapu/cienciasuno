package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import avl.Arbol;
import rojinegro.ArbolRN;

public class IPrincipalBotones implements ActionListener {
	private VPrincipal principal;
	private ArbolRN primerNodo;

	public IPrincipalBotones(VPrincipal principal) {
		this.principal = principal;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton obj = (JButton) e.getSource();
		for (int i = 0; i < principal.btnPrincipal.length; i++) {
			if (principal.btnPrincipal[i].equals(obj)) {
				try {
					accion(i);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}

			}
		}
	}

	private void insertaUnNodo(String texto, int[] arr) {
		int numero = Integer.parseInt(texto);
		ArbolRN nuevoNodo;

		boolean esHijoDer;
		arr[4]++;
		if (primerNodo == null) // ve si el arbol esta vacio y crea el nodo como raiz
		{
			primerNodo = new ArbolRN(texto, null);
			primerNodo.setColor(false);
		} else {

			nuevoNodo = new ArbolRN(texto, primerNodo);
			while (true) {

				// recorre los nodos que existan para buscar el lugar del nuevo nodo en base a
				// su numero
				arr[4]++;
				if (numero < nuevoNodo.getPadre().getValorEntero()) {
					arr[4]++;
					if (nuevoNodo.getPadre().getIzquierda() != null) {
						nuevoNodo.setPadre(nuevoNodo.getPadre().getIzquierda());
					} else {
						nuevoNodo.getPadre().setIzquierda(nuevoNodo);
						esHijoDer = false;
						break;
					}
				} else if (numero > nuevoNodo.getPadre().getValorEntero()) {
					arr[4]++;
					if (nuevoNodo.getPadre().getDerecha() != null) {
						nuevoNodo.setPadre(nuevoNodo.getPadre().getDerecha());
					} else {
						nuevoNodo.getPadre().setDerecha(nuevoNodo);
						esHijoDer = true;
						break;
					}

				} else if (numero == nuevoNodo.getPadre().getValorEntero()) {
					arr[4]++;
					JOptionPane.showMessageDialog(null, "El número ya fue ingresado antes");
					esHijoDer = false;
					break;
				}
			}
			;

			// en caso de que se presente que el padre del nuevo nodo es rojo envia al
			// metodo que soluciona esto
			arr[4]++;
			if (ArbolRN.isColor(nuevoNodo.getPadre())) {
				casoRojoRojo(nuevoNodo.getPadre(), esHijoDer);
			}
		}
	}

	// nodo es rojo
	private void casoRojoRojo(ArbolRN n, boolean hijoDer) {
		ArbolRN padreDePadre = n.getPadre();

		ArbolRN hermanoDePadre;

		ArbolRN temporal;

		if (padreDePadre.getIzquierda() != null && padreDePadre.getDerecha() != null) {

			// caso uno y dos: volver a colorear
			if (n == padreDePadre.getIzquierda()) {
				hermanoDePadre = padreDePadre.getDerecha();
			} else {
				hermanoDePadre = padreDePadre.getIzquierda();
			}

			if (ArbolRN.isColor(hermanoDePadre)) {

				hermanoDePadre.setColor(false);
				n.setColor(false);

				if (padreDePadre != primerNodo) {
					padreDePadre.setColor(true);
				}
				if (padreDePadre.getPadre() != null) {

					if (ArbolRN.isColor(padreDePadre.getPadre())) // revisar que no se haya creado un caso rojo-rojo
																	// hacia arriba
					{
						casoRojoRojo(padreDePadre.getPadre(), padreDePadre.getPadre().getIzquierda() != padreDePadre);
					}

				}

				return;

			}

		}

		if (!hijoDer && padreDePadre.getIzquierda() == n) {

			// caso tres: reestructurar
			n.setColor(false);
			padreDePadre.setColor(true);

			temporal = n.getDerecha();
			n.setDerecha(padreDePadre);
			n.setPadre(padreDePadre.getPadre());

			padreDePadre.setPadre(n);
			padreDePadre.setIzquierda(temporal);

			if (temporal != null) {
				temporal.setPadre(padreDePadre);
			}

			if (n.getPadre() != null) {

				temporal = n.getPadre();

				if (temporal.getIzquierda() == n.getDerecha()) {
					temporal.setIzquierda(n);
				} else {
					temporal.setDerecha(n);
				}

			} else {
				primerNodo = n;
			}

		} else if (hijoDer && padreDePadre.getDerecha() == n) {

			// caso cuatro: reestructurar
			n.setColor(false);
			padreDePadre.setColor(true);

			temporal = n.getIzquierda();
			n.setIzquierda(padreDePadre);
			n.setPadre(padreDePadre.getPadre());

			padreDePadre.setPadre(n);
			padreDePadre.setDerecha(temporal);

			if (temporal != null) {
				temporal.setPadre(padreDePadre);
			}

			if (n.getPadre() != null) {

				temporal = n.getPadre();

				if (temporal.getIzquierda() == n.getIzquierda()) {
					temporal.setIzquierda(n);
				} else {
					temporal.setDerecha(n);
				}

			} else {
				primerNodo = n;
			}

		} else if (hijoDer && padreDePadre.getIzquierda() == n) {

			// caso cinco: reestructurar
			hermanoDePadre = n.getDerecha();
			temporal = hermanoDePadre.getIzquierda();
			padreDePadre.setIzquierda(hermanoDePadre);

			hermanoDePadre.setPadre(padreDePadre);
			hermanoDePadre.setIzquierda(n);
			n.setPadre(hermanoDePadre);

			n.setDerecha(temporal);

			if (temporal != null) {
				temporal.setPadre(n);
			}

			// lleva al caso tres
			casoRojoRojo(hermanoDePadre, false);

		} else if (!hijoDer && padreDePadre.getDerecha() == n) {

			// caso seis: reestructurar
			hermanoDePadre = n.getIzquierda();
			temporal = hermanoDePadre.getDerecha();
			padreDePadre.setDerecha(hermanoDePadre);

			hermanoDePadre.setPadre(padreDePadre);
			hermanoDePadre.setDerecha(n);
			n.setPadre(hermanoDePadre);

			n.setIzquierda(temporal);

			if (temporal != null) {
				temporal.setPadre(n);
			}

			// lleva al caso cuatro
			casoRojoRojo(hermanoDePadre, true);

		}

	}

	private void accion(int num) {
		String[] otroOrden = null;
		int[] arr = { 0, 0, 0, 0, 0 };
		int[] arre = { 0, 0, 0, 0, 0 };
		switch (num) {
		case 0:
			primerNodo = null;
			otroOrden = randomizar(100);
			Arbol tree = Arbol.reconstruir(otroOrden, arr);
			for (int i = 0; i < otroOrden.length; i++) {
				insertaUnNodo(otroOrden[i], arre);
			}
			VArbol vtnTr = new VArbol(primerNodo, arre);
			JOptionPane.showMessageDialog(null,
					"Se evaluaron " + Integer.toString(arre[4]) + " condicionales para el rojinegro");
			vtnTr.setVisible(true);

			VArbol vtnTree = new VArbol(tree, arr);
			JOptionPane.showMessageDialog(null,
					"Se evaluaron " + Integer.toString(arr[4]) + " condicionales para el AVL");
			vtnTree.setVisible(true);

			break;
		case 1:
			principal.dispose();
			System.exit(0);
			break;
		case 2:
			break;
		default:
			System.out.println("Opción no disponible");
			break;
		}
	}

	String imprimir(ArrayList<String> lista) {
		String resultado = "";
		for (int i = 0; i < lista.size(); i++) {
			resultado = resultado + lista.get(i);
			if (i + 1 < lista.size()) {
				resultado = resultado + ",";
			}
		}
		return resultado;
	}

	String[] randomizar(int i) {
		String[] resultado = new String[i];
		ArrayList<String> are = new ArrayList<String>();
		while (are.size() < i) {
			boolean fl = true;
			int ran = (int) ((Math.random() * (-i * 10)) + i * 10);
			for (int j = 0; (j < are.size() && fl); j++) {
				if (Integer.parseInt(are.get(j)) == ran) {
					fl = false;
				}
			}
			if (fl) {
				are.add(Integer.toString(ran));
			}
		}
		for (int j = 0; j < are.size(); j++) {
			resultado[j] = are.get(j);
		}
		return resultado;
	}
}
