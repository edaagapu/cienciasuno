package interfaz;

import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class VPrincipal extends JFrame {

	private JPanel contentPane;
	private String[] textoBotones = { "Aleatorio", "Salir"};

	public int seleccion;
	public JTextField[] txtCampos = new JTextField[1];
	public JButton[] btnPrincipal = new JButton[textoBotones.length];

	public VPrincipal() {
		IPrincipalBotones ctrBoton = new IPrincipalBotones(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInorden = new JLabel("Construcción de Árboles");
		lblInorden.setBounds(10, 20, 190, 20);

		contentPane.add(lblInorden);

		for (int i = 0; i < txtCampos.length; i++) {
			txtCampos[i] = new JTextField();
			txtCampos[i].setBounds(10 + (290 * i), 60, 230, 20);
			txtCampos[i].setBorder(new LineBorder(Color.BLACK));
			contentPane.add(txtCampos[i]);
			txtCampos[i].setVisible(false);
		}

		for (int i = 0; i < btnPrincipal.length; i++) {
			btnPrincipal[i] = new JButton(textoBotones[i]);
			btnPrincipal[i].setBorder(new LineBorder(Color.BLACK));
			btnPrincipal[i].setBackground(Color.WHITE);
			btnPrincipal[i].setBounds(20 + (120 * i), 100, 90, 30);
			btnPrincipal[i].addActionListener(ctrBoton);
			contentPane.add(btnPrincipal[i]);
		}
		setBounds(40, 60, 20 + 120 * btnPrincipal.length, 170);

	}
}
