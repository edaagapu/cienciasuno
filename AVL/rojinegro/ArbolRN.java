package rojinegro;

import avl.Arbol;

public class ArbolRN {
	private ArbolRN padre;
	private ArbolRN izquierda;
	private ArbolRN derecha;
	private String valor;
	private boolean color; // (True) Rojo - (False) Negro

	public ArbolRN(String valor, ArbolRN padre) {
		setValor(valor);
		setPadre(padre);
		setIzquierda(null);
		setDerecha(null);
		color = true;
	}

	public int cantNiveles() {
		ArbolRN tempo = this;
		int recorrido = 1, recIzq = 0, recDer = 0;
		if (tempo.getIzquierda() != null || tempo.getDerecha() != null) {
			if (tempo.getIzquierda() != null) {
				recIzq = tempo.getIzquierda().cantNiveles();
			}
			if (tempo.getDerecha() != null) {
				recDer = tempo.getDerecha().cantNiveles();
			}
			if (recDer < recIzq) {
				return recorrido + recIzq;
			} else {
				return recorrido + recDer;
			}
		} else {
			return 0;
		}
	}

	public ArbolRN getPadre() {
		return padre;
	}

	public void setPadre(ArbolRN padre) {
		this.padre = padre;
	}

	public ArbolRN getIzquierda() {
		return izquierda;
	}

	public void setIzquierda(ArbolRN izquierda) {
		this.izquierda = izquierda;
	}

	public ArbolRN getDerecha() {
		return derecha;
	}

	public void setDerecha(ArbolRN derecha) {
		this.derecha = derecha;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public int getValorEntero() {
		return Integer.parseInt(valor);
	}

	public static boolean isColor(ArbolRN arbol) {
		if (arbol == null) {
			return false;
		} else {
			return arbol.color;
		}
	}

	public void setColor(boolean color) {
		this.color = color;
	}

	public static ArbolRN copiar(ArbolRN arbol) {
		if (arbol != null) {
			ArbolRN resultado = new ArbolRN(arbol.getValor(), arbol.getPadre());
			resultado.setDerecha(ArbolRN.copiar(arbol.getDerecha()));
			resultado.setIzquierda(ArbolRN.copiar(arbol.getIzquierda()));
			return resultado;
		} else {
			return null;
		}
	}

	// Balancear

	// soluciona si al insertar se presenta un caso donde el nodo padre del nuevo

	public void balancearArbolRN(ArbolRN arbol) {
		if (ArbolRN.isColor(arbol.getPadre()) && ArbolRN.isColor(arbol)) {
			ArbolRN cent = copiar(arbol.getPadre());
			// Caso 1
			/*
			 * Padre y tio: Rojo Abuelo: Negro
			 */
			if (!isColor(cent.getPadre()) && cent.getPadre() != null) {

				cent.getPadre().setColor(true);
				if (color) {

				}
				cent.getPadre().getDerecha().setColor(false);
			}
		} else {
			balancearArbolRN(arbol.getDerecha());
			balancearArbolRN(arbol.getIzquierda());
		}
	}

}
