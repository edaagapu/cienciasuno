package lista;

public class Lista {
	private Nodo cabecera;
	private Nodo centinela;

	public Lista() {
		cabecera = new Nodo();
		centinela = null;
	}

	public boolean estaVacio() {
		return cabecera.getSiguiente() == null;
	}

	public boolean agregar(int valor) {
		Nodo tempo = new Nodo(valor);
		Nodo flag = cabecera;
		centinela = cabecera.getSiguiente();
		while (centinela != null && centinela.getValor() < tempo.getValor()) {
			flag = centinela;
			centinela = centinela.getSiguiente();
		}
		if (centinela != null) {
			if (tempo.getValor() == centinela.getValor()) {
				return false;
			}
			tempo.setSiguiente(centinela);
		}
		flag.setSiguiente(tempo);
		return true;
	}

	public boolean eliminar(int valor) {
		Nodo flag = null;
		centinela = cabecera;
		while (centinela.getSiguiente() != null && centinela.getValor() < valor) {
			flag = centinela;
			centinela = centinela.getSiguiente();
		}
		if (valor == centinela.getValor()) {
			flag.setSiguiente(centinela.getSiguiente());
			return true;
		} else {
			return false;
		}
	}

	public String imprimir() {
		String resultado = "";
		centinela = cabecera.getSiguiente();
		while (centinela != null) {
			resultado = resultado + "(" + Integer.toString(centinela.getValor()) + ")";
			centinela = centinela.getSiguiente();
			if (centinela != null) {
				resultado = resultado + "->";
			}
		}
		return resultado;
	}
}
