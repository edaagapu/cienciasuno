package lista;

public class Nodo {
	private int valor;
	private Nodo siguiente;
	
	public Nodo() {
		siguiente = null;
		valor = -1;
	}
	
	public Nodo(int valor) {
		this.valor = valor;
		siguiente = null;
	}
	
	public Nodo getSiguiente() {
		return siguiente;
	}
	public void setSiguiente(Nodo siguiente) {
		this.siguiente = siguiente;
	}
	
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	
	
}
