package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextArea;

public class VPrincipal extends JFrame {

	private JPanel contentPane;
	private String[] nameBoton = { "Agregar", "Eliminar", "Imprimir" };
	public JButton[] botones = new JButton[nameBoton.length];
	private JPanel pnlPrincipal;
	private JPanel pnlTexto;
	public JTextArea txtpnlInformacion;

	public VPrincipal() {
		IPrincipal controlador = new IPrincipal(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 390, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(Color.WHITE);
		pnlPrincipal.setBorder(new LineBorder(Color.BLACK, 1, true));
		pnlPrincipal.setBounds(10, 10, 370, 250);
		pnlPrincipal.setLayout(null);
		contentPane.add(pnlPrincipal);

		for (int i = 0; i < botones.length; i++) {
			botones[i] = new JButton(nameBoton[i]);
			botones[i].setBorder(new LineBorder(Color.BLACK));
			botones[i].setBackground(Color.WHITE);
			botones[i].addActionListener(controlador);
			botones[i].setBounds(10 + (120 * i), 10, 110, 25);
			pnlPrincipal.add(botones[i]);
		}

		pnlTexto = new JPanel();
		pnlTexto.setBorder(new LineBorder(Color.DARK_GRAY));
		pnlTexto.setBackground(Color.WHITE);
		pnlTexto.setBounds(10, 45, 350, 185);
		pnlTexto.setLayout(null);
		pnlPrincipal.add(pnlTexto);

		txtpnlInformacion = new JTextArea();
		txtpnlInformacion.setLineWrap(true);
		txtpnlInformacion.setWrapStyleWord(true);
		txtpnlInformacion.setEditable(false);
		txtpnlInformacion.setBorder(null);
		txtpnlInformacion.setBounds(10, 10, 330, 165);
		pnlTexto.add(txtpnlInformacion);
	}
}
