package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import lista.Lista;

public class IPrincipal implements ActionListener {

	private VPrincipal ventana = null;
	private Lista lista;

	// { "Agregar", "Eliminar", "Imprimir" }
	public IPrincipal(VPrincipal ventana) {
		this.ventana = ventana;
		lista = new Lista();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		String dato;
		if (boton.equals(ventana.botones[0])) {
			dato = JOptionPane.showInputDialog("Escriba el dato a ingresar");
			if (lista.agregar(Integer.parseInt(dato))) {
				JOptionPane.showMessageDialog(null, "El dato se agregó con éxito");
			} else {
				JOptionPane.showMessageDialog(null, "El dato no se pudo agregar");
			}
			agregarTexto();
		} else if (boton.equals(ventana.botones[1])) {
			dato = JOptionPane.showInputDialog("Escriba el dato a borrar");
			if (lista.eliminar(Integer.parseInt(dato))) {
				JOptionPane.showMessageDialog(null, "El dato se eliminó con éxito");
			} else {
				JOptionPane.showMessageDialog(null, "El dato no se encontró en la lista");
			}
			agregarTexto();
		} else if (boton.equals(ventana.botones[2])) {
			agregarTexto();
		}
	}

	public void agregarTexto() {
		ventana.txtpnlInformacion.setText(lista.imprimir());
	}
}
