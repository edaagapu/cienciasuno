console.log("Hola mundo")
/*
Esta funcion devuelve el numero de digitos del
numero mas largo
*/
function getMaxLen (arr){
    let max = 0;
    for (let elem of arr){
        if (max < elem.toString().length){
            max = elem.toString().length;
        }
    }
    return max
}
/*
Esto devuelve el digito en la posicion requerida de un numero
*/
function getNumPos (num, pos){
    return Math.floor(Math.abs(num)/Math.pow(10,pos)% 10);
}
function radixSort (arr){
    const max = getMaxLen (arr);
    for (let i=0; i<max; i++){
        let matriz = Array.from({length:10},()=>[]);
        for (let j = 0; j < arr.length; j++){
            matriz [getNumPos(arr[j], i)].push(arr[j]);
        }
        arr = [].concat(...matriz);
    }
    return arr
}
function aleatorio(min,max){
    return Math.floor(Math.random() * (max - min)) + min;
}
console.log(radixSort([5,57,7,3,933]))
function peorCaso(){

}
function mejorCaso(){

}
function casoMedio(){
   var n = document.getElementById("N").value;
    //var n = 7
	var arr = [];
    for (let i=0;i<n;i++){
        arr.push(aleatorio(1,n*10));
    }
    arr=radixSort(arr);
    console.log(arr);
}
casoMedio();