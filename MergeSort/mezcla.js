/*
@author: Edwin Aarón Garcia Pulido
@author: Juan Camilo Navarro Quiroga

Archivo capaz de hacer las mezclas requeridas en el MergeSort
*/
var conteo=0;

    function sinRepetir(arreglo){
        let resultado = [...new Set(arreglo)];
        return resultado;
    }
    
    function generarMejorCaso(tamano){
        let arreglo = [];
        while(arreglo.length<tamano){    
            for(var i=arreglo.length;i<tamano;i++){
                arreglo.push(Math.floor(Math.random()*(100)+1));
            }
            arreglo = sinRepetir(arreglo);
        }
        arreglo.sort(function(a, b){return a-b});
        return arreglo;
    }

    function generarPeorCaso(tamano){
        let arreglo = [];
        let resultado = [];    
        while(arreglo.length<tamano){    
            for(var i=arreglo.length;i<tamano;i++){
                arreglo.push(Math.floor(Math.random()*(100)+1));
            }
            arreglo = sinRepetir(arreglo);
        }
        arreglo.sort(function(a, b){return a-b});
        for(var i=0;i<tamano;i++){
            resultado[i] = arreglo[tamano-(i+1)];
        }
        return resultado;
    }

    function generarMedioCaso(tamano){
        let arreglo = [];
        while(arreglo.length<tamano){
            arreglo.push(Math.floor(Math.random()*(100)+1));
            arreglo = sinRepetir(arreglo);
        }
        return arreglo;
    }


    function mergeSort(arreglo){
        let resultado = [];
        if (arreglo.length==1){
            resultado.push(arreglo.shift());
        }
        else{
            let derecha = [];
            let izquierda = [];
            for (i=0;i<arreglo.length/2;i++){
                derecha.push(arreglo[i]);            
                if(Math.round(arreglo.length/2)+i<arreglo.length){                
                    izquierda.push(arreglo[Math.round(arreglo.length/2)+i]);
                }         
            }
            let der = mergeSort(derecha);
            let izq = mergeSort(izquierda);
                     
            while (der.length!=0 || izq.length!=0){
                if (der.length==0){
                    while (izq.length!=0){
                        conteo++;                        
                        resultado.push(izq.shift());                    
                    }                
                } else if (izq.length==0){
                    while (der.length!=0){
                        conteo++;
                        resultado.push(der.shift());                    
                    }
                } else {
                    conteo++;
                    if (der[0]<izq[0]){
                        resultado.push(der.shift());            
                    }
                    else{
                        resultado.push(izq.shift());            
                    }
                }                            
            }
        }
        return resultado;
    }

    function organizar(arreglo){
        let solucion = document.getElementById("solucion");
        let contar = document.getElementById("conteo");
        let original = document.getElementById("original");
        let formula = document.getElementById("formula");
        let form = arreglo.length*(Math.log(arreglo.length)/Math.LN2);
        conteo = 0;            

        original.innerHTML = "Arreglo Original: ["+arreglo.toString()+"]";
        solucion.innerHTML = "Arreglo Organizado: ["+mergeSort(arreglo).toString()+"]";
        contar.innerHTML = "Conteo: "+conteo.toString(); 
        formula.innerHTML = "Formula: "+form.toString(); 
    }

    function organizarPeor(){
        let tamano = parseInt(document.getElementById("tamano").value);
        organizar(generarPeorCaso(tamano));    
    }

    function organizarMejor(){
        let tamano = parseInt(document.getElementById("tamano").value);
        organizar(generarMejorCaso(tamano));    
    }

    function organizarMedio(){
        let tamano = parseInt(document.getElementById("tamano").value);
        organizar(generarMedioCaso(tamano));    
    }
