function limpiar(titleCont){
  let contenedor = document.getElementById(titleCont+"contenedor");
  if (contenedor){
    contenedor.parentNode.removeChild(contenedor);
  }
}

function crearCondIni(){
    let titleCont ="condIni";
    limpiar(titleCont);
    let tam = parseInt(document.getElementById("ordenEcuacion").value)+1;
    let principal =  document.getElementById(titleCont);
    let contenedor = document.createElement("div");
    contenedor.id = titleCont+"contenedor";
    principal.appendChild(contenedor);
    let texto = "";
    for (let i=0;i<tam-1;i++){
        texto = texto + "F(<input type=\"text\" id=\"condIni0-"+i.toString()+"\" size=\"1\">) = <input type=\"text\" id=\"condIni1-"+i.toString()+"\" size=\"1\">"+"<br>";
    }
    texto = texto + "<br>";
    principal.innerHTML = texto;
}

function crearCoef(){
    let titleCont ="coef";
    limpiar(titleCont);
    let tam = parseInt(document.getElementById("ordenEcuacion").value)+1;
    let principal =  document.getElementById(titleCont);
    let contenedor = document.createElement("div");
    contenedor.id = titleCont+"contenedor";
    principal.appendChild(contenedor);
    let texto = "";
    for (let i=0;i<tam;i++){
        if(i<tam-2){
          texto = texto + "<input type=\"text\" id=\"coef"+i.toString()+"\" size=\"1\"> X^"+(tam-(i+1)).toString();
        } else if(i==tam-2){
          texto = texto + "<input type=\"text\" id=\"coef"+i.toString()+"\" size=\"1\"> X";
        } else if(i==tam-1){
          texto = texto + "<input type=\"text\" id=\"coef"+i.toString()+"\" size=\"1\">";
        }
        if(i<tam-1){
          texto = texto + " + ";
        }
    }
    texto = texto + "<br><br>";
    principal.innerHTML = texto;
}

//Falta la funcion para generar los espacios para llenar
function generar(){
    limpiar();
    crearCondIni();
    crearCoef();
}

//Falta funcion para obtener los datos y así poder calcular
function calcular(){
  let tam = parseInt(document.getElementById("ordenEcuacion").value)+1;
  let matrizCondIni = [];
  for (let i = 0; i < tam-1; i++) {
     let filaCondIni = [];
     for (let j = 0; j < 2; j++) {
       let dato = parseFloat(document.getElementById("condIni"+j.toString()+"-"+i.toString()).value);
       filaCondIni.push(dato);
     }
     matrizCondIni.push(filaCondIni);
  }
  let coeficientes = [];
  for (let i = 0; i < tam; i++) {
    let dato = parseFloat(document.getElementById("coef"+i.toString()).value);
    coeficientes.push(dato);
  }
  let resultado = hallarEcuacion(coeficientes,matrizCondIni);
  let res = document.getElementById("ecuacionResultado");
  res.innerHTML = "<h2>"+resultado+"</h2>";
}

function hallarRaices(arr){
  if(arr.length<=3){
    if (arr.length==2) {
      return [(arr[1]/arr[0])*(-1)];
    } else if (arr.length==3) {
      return cuadratica(arr);
    }
  } else {
    let numerador = factores(arr[arr.length-1]);
    let denominador = factores(arr[0]);
    let posDiv = obtDivisores(numerador,denominador);
    let raices = obtRaices(arr,posDiv);
    if (raices.length<arr.length-1){
      for (let i = 0; i < raices.length; i++) {
        arr = reducir(arr,raices[i]);
      }
      let resul = raices.concat(hallarRaices(arr));
      return resul;
    }
    return raices;
  }
}

function cuadratica(arr){
  let a = arr[0];
  let b = arr[1];
  let c = arr[2];

  let evImg = Math.pow(b,2)-(4*a*c);
  if (evImg < 0) {
    return [];
  }
  let evReal = ((-1)*b)/(2*a);
  evImg = Math.pow(evImg,0.5);
  evImg = evImg/(2*a);
  if(evImg == 0 && c==0){
    return [0,evReal];
  } else {
    return [evReal-evImg,evReal+evImg];
  }
}

//Funciones de Newton-Raphson
function hallarRaicesRaphson(arr){
  if(arr.length<=3){
    if (arr.length==2) {
      return [(arr[1]/arr[0])*(-1)];
    } else if (arr.length==3) {
      return cuadratica(arr);
    }
  } else {
    let raiz = newtonRaphson(arr,4);
    arr = reducir(arr,raiz);
    let raices = [raiz];
    raices = raices.concat(hallarRaicesRaphson(arr));
    return raices;
  }
}

function derivar(px){
	primx = [];
  for (let i = 0; i < px.length-1; i++) {
    primx.push(px[i]*(px.length-(i+1)));
  }
	return primx;
}

function newtonRaphson(px,xi){
	prx = derivar(px);
	form = fx(px,xi)/fx(prx,xi);
	while (form!=0){
		xi = Math.round((xi - form)*10000)/10000;
		form = Math.round((fx(px,xi)/fx(prx,xi))*10000)/10000;
  }
  return Math.round(xi*1000)/1000;
}

//Fin Funciones de Newton-Raphson

function reducir(arreglo,num){
  let reduccion = [];
  reduccion.push(arreglo[0]);
  for (let i = 1; i < arreglo.length; i++) {
    reduccion.push(arreglo[i]+reduccion[i-1]*num);
  }
  reduccion.pop();
  return reduccion;
}

function obtRaices(px,divisores){
  let raices = [];
  for (let i = 0; i < divisores.length; i++) {
    if(Math.round(fx(px,divisores[i]))==0){
      raices.push(divisores[i]);
    }
  }
  return raices;
}
function fx(px,num){
  let resultado = 0
  for (let i = 0; i < px.length; i++) {
    resultado = resultado + px[i]*Math.pow(num,px.length-(i+1));
  }
  return resultado
}

function obtDivisores(numerador,denominador){
  let posD = [];
  for (let i = 0; i < denominador.length; i++) {
    for (let j = 0; j < numerador.length; j++) {
        posD.push(numerador[j]/denominador[i]);
    }
  }
  return sinRepetir(posD);
}

function sinRepetir(arreglo){
    let resultado = [...new Set(arreglo)];
    return resultado;
}

function factores(numero){
  let factores = [];
  factores.push(1);
  factores.push(-1);
  for (let i = 2; i < numero; i++) {
   if (numero%i==0) {
     factores.push(i);
     factores.push(-i);
   }
 }
 factores.push(numero);
 factores.push((-1)*numero);
 return factores;
}

function hallarEcuacion(arr,inicial){
  let raices = hallarRaicesRaphson(arr);
  let ecuacion = "f(n) = ";
  //Organiza las raices, para saber si hay repetidas
  raices.sort(function(a,b){return a-b});
  //Crea la matriz de coeficientes para hallar el valor de cada uno
  let matrizCoef = [];
  let vectorRecur = [];
  for (let i = 0; i < raices.length; i++) {
    matrizCoef.push([]);
    for (let j = 0; j < raices.length; j++) {
      let dato;
      if (j > 0 && raices[j] == raices[j-1]) {
        //Si posee raices repetidas, multiplica el valor anterior por n
        dato=matrizCoef[i][matrizCoef[i].length-1]*inicial[i][0];
      }else{
        //Hace la operacion (raiz^n)
        dato=Math.pow(raices[j],inicial[i][0]);
      }
      matrizCoef[i].push(parseFloat(dato.toString()));
    }

    //Vector con los valores de T(n)
    let valor = inicial[i][1];
    vectorRecur.push(parseFloat(valor.toString()));
  }
  let coeficientes=gaussJordan(matrizCoef,vectorRecur);
  let conteo = 1;
  for (let i = 0; i < coeficientes.length; i++) {
    if (i > 0 && raices[i] == raices[i-1]) {
      //Si posee raices repetidas, multiplica el valor anterior por n
      ecuacion = ecuacion + " (" + Math.round(coeficientes[i]*100)/100 + ")" + "(n^" + conteo.toString() +")" + "(" + Math.round(raices[i]*100)/100 + "^n) ";
      conteo = conteo+1;
    }else{
      //Hace la operacion (raiz^n)
      ecuacion = ecuacion + " (" + Math.round(coeficientes[i]*100)/100 + ")" + "(" + Math.round(raices[i]*100)/100 + "^n) ";
      conteo = 1;
    }
    if(i<coeficientes.length-1){
      ecuacion = ecuacion+"+";
    }
  }
    return ecuacion;
}

function gaussJordan(matriz,vector){
  let coeficientes = matriz.slice();
  let recursos = vector.slice();
  let ind = 0;
  while(!validar(coeficientes) && ind<recursos.length){
    reducirFila(coeficientes,recursos,ind);
    operarFila(coeficientes,recursos,ind);
    ind = ind+1;
  }
  return recursos;
}
//Funcion para Gauss-Jordan
function operarFila(matriz,vector,indice){
  for (let i = 0; i < matriz.length; i++) {
    if (i!=indice) {
      let flag = matriz[i][indice];
      for (let j = 0; j < matriz[i].length; j++) {
        let oper = matriz[indice][j]*flag
        matriz[i][j] = matriz[i][j]-oper;
      }
      vector[i] = vector[i]-(vector[indice]*flag);
    }
  }
}
//Funcion para Gauss-Jordan
function reducirFila(matriz,vector,indice){
  let flag = matriz[indice][indice];
  if(flag!=1){
    for (let i = 0; i < matriz[indice].length; i++) {
      matriz[indice][i] = matriz[indice][i]/flag;
    }
    vector[indice] = vector[indice]/flag;
  }
}
//Funcion para Gauss-Jordan
function validar(matriz){
  let flag = true;
  for (let i = 0; (i <matriz.length && flag); i++) {
    for (let j = 0; (j <matriz.length && flag); j++) {
      if(j==i){
        flag = (matriz[i][j]==1);
      }else{
        flag = (matriz[i][j]==0);
      }
    }
  }
  return flag;
}
