/*
@author: Edwin Aarón Garcia Pulido
@author: Juan Camilo Navarro Quiroga

Archivo con funciones capaces de analizar la complejidad de un arreglo generado de forma aleatoria para el peor caso, el caso medio y el mejor caso.
*/
    function sinRepetir(arreglo){
        var resultado = [...new Set(arreglo)];
        return resultado;
    }

    function generarPeorCaso(){
        var arreglo = [];
        var tamano = 0;    
        var resultado = [];    
        tamano = parseInt(document.getElementById("tamano").value);
        while(arreglo.length<tamano){    
            for(var i=arreglo.length;i<tamano;i++){
                arreglo.push(Math.floor(Math.random()*(100)+1));
            }
            arreglo = sinRepetir(arreglo);
        }
        arreglo.sort(function(a, b){return a-b});
        for(var i=0;i<tamano;i++){
            resultado[i] = arreglo[tamano-(i+1)];
        }
        return resultado;
    }

    function generarMedioCaso(){
        arreglo = [];
        var tamano = 0;
        tamano = parseInt(document.getElementById("tamano").value);
        for(var i=0;i<tamano && arreglo.length<tamano;i++){
            arreglo.push(Math.floor(Math.random()*(100)+1));
            sinRepetir(arreglo);
        }
        return arreglo;
    }

    function generarMejorCaso(){
        var arreglo = [];
        var tamano = 0;    
        var resultado = [];    
        tamano = parseInt(document.getElementById("tamano").value);
        while(arreglo.length<tamano){    
            for(var i=arreglo.length;i<tamano;i++){
                arreglo.push(Math.floor(Math.random()*(100)+1));
                sinRepetir(arreglo);
            }
        }
        arreglo.sort(function(a, b){return a-b});
        return arreglo;
    }
    
    function calcularPeorCaso(){
        var contador = 0;
        var tamano = 0;
        tamano = parseInt(document.getElementById("tamano").value);
        arreglo = generarPeorCaso();
        console.log("Hola: "+arreglo);
        for (var i=0;i<tamano-1;i++){
            for (var j=i+1;j<tamano;j++){
                if(arreglo[i]>arreglo[j]){           
                    var tempo = arreglo[j];
                    arreglo[j] = arreglo[i];
                    arreglo[i] = tempo;        
                    contador++;
                    contador++;            
                }
            }
        }
        var formula = document.getElementById("formula");
        formula.innerHTML = "Fórmula: "+ (tamano*(tamano - 1));
        var cont = document.getElementById("conteo");
        cont.innerHTML = "Conteo: "+contador;
    }

    function calcularMedioCaso(){
        var contador = 0;
        var tamano = 0;    
        tamano = parseInt(document.getElementById("tamano").value);
        arreglo = generarMedioCaso();
        for (var i=0;i<tamano-1;i++){
            for (var j=i+1;j<tamano;j++){
                if(arreglo[i]>arreglo[j]){           
                    var tempo = arreglo[j];
                    arreglo[j] = arreglo[i];
                    arreglo[i] = tempo;
                    contador++;
                }
            }    
        }
        var formula = document.getElementById("formula");
        formula.innerHTML = "Fórmula: "+ ((tamano*(tamano-1))/2);
        var cont = document.getElementById("conteo");
        cont.innerHTML = "Conteo: "+contador;
    }

    function calcularMejorCaso(){
        var contador = 0;
        arreglo = generarMejorCaso();
        for (var i=0;i<tamano-1;i++){
            for (var j=i+1;j<tamano;j++){
                if(arreglo[i]>arreglo[j]){           
                    var tempo = arreglo[j];
                    arreglo[j] = arreglo[i];
                    arreglo[i] = tempo;
                    contador++;        
                }
            }    
        }
        var formula = document.getElementById("formula");
        formula.innerHTML = "Fórmula: "+ 0;
        var cont = document.getElementById("conteo");
        cont.innerHTML = "Conteo: "+contador;
    }