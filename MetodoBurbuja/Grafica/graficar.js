/*
@author:    Edwin Aaron Garcia Pulido
            Juan Camilo Navarro Quiroga

Archivo mediante el cual se grafica la complejidad del Método Burbuja.
*/

function inicialize(c){
    c.width=c.width;
}

function graficar(){
    var c = document.getElementById("lienzo");
    var ctx = c.getContext("2d");
    inicialize(c);
    var tamano = parseInt(document.getElementById("tamano").value);
    
    var modulo = 1;
    if(tamano>=30 && tamano<100){
        modulo=5;
    } else if(tamano>=100){
        modulo=10;    
    }
    var tam=tamano/modulo;

    ctx.moveTo(20, 20);
    ctx.lineTo(20, 420);
    ctx.stroke();
    ctx.moveTo(20, 420);
    ctx.lineTo(420, 420);
    ctx.stroke();
    
    ctx.strokeText("Tamaño",425,420);
    //Eje X  
    for (var i=0;i<=tam;i++){
        ctx.moveTo(20+(i*(400/(tam+1))),420);
        ctx.lineTo(20+(i*(400/(tam+1))),425);
        ctx.stroke();
        ctx.font = "10px Algerian";
        ctx.strokeText((i*modulo).toString(),17+(i*(400/(tam+1))),435);
    }

    //Eje Y
    for (var i=0;i<=tam;i++){
        ctx.moveTo(20,420-(i*(400/(tam+1))));
        ctx.lineTo(15,420-(i*(400/(tam+1))));
        ctx.stroke();
        ctx.font = "10px Algerian";
        ctx.strokeText((i*modulo).toString(),3,423-(i*(400/(tam+1))));
    }
    ctx.strokeText("O(N)",10,10);
    graficarPuntos(ctx,tamano);
}

function graficarPuntos(ctx,tamano){
    ctx.fillStyle="rgb(200,10,150)";
    for(var i=1;i*((i-1)/2)<tamano;i=i+0.007){
        ctx.moveTo(20+i*(400/(tamano+1)), 420-(i*(i-1)*(200/(tamano+1))));                
        ctx.arc(20+i*(400/(tamano+1)), 420-(i*(i-1)*(200/(tamano+1))),0.7,0,2*Math.PI,true);
        ctx.fill();
    }
}