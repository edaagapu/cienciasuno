/*
@author: Edwin Aarón Garcia Pulido
@author: Juan Camilo Navarro Quiroga

Archivo con funciones capaces de analizar la complejidad de un arreglo generado de forma aleatoria para el peor caso, el caso medio y el mejor caso.
*/
var contadorStrassen = 0;


    function sumar(matrizU,matrizD){
        var resultado = new Array(matrizU.length);        
        for (i=0;i<resultado.length;i++){
            resultado[i] = new Array(matrizU[i].length);
            for (j=0;j<resultado[i].length;j++){
                resultado[i][j] = matrizU[i][j] + matrizD[i][j];
            }        
        }
        return resultado;
    }

    function restar(matrizU,matrizD){
        var resultado = new Array(matrizU.length);        
        for (i=0;i<resultado.length;i++){
            resultado[i] = new Array(matrizU[i].length);
            for (j=0;j<resultado[i].length;j++){
                resultado[i][j] = matrizU[i][j] - matrizD[i][j];           
            }        
        }
        return resultado;
    }

    function multMatriz(matrizU,matrizD){
        var cont=0;        
        var multiplicacion = new Array(matrizU.length)        
        for (i=0; i < matrizU.length; i++) {
            multiplicacion[i] = new Array(matrizU.length);
            for (j=0; j < matrizU[i].length; j++) {
                multiplicacion[i][j] = 0;
                for (k=0; k<matrizU.length; k++) {
                    multiplicacion [i][j] += matrizU[i][k]*matrizD[k][j];
                    cont++;
                }
            }
        }
        var imp = document.getElementById("normal");
        imp.innerHTML=cont.toString();   
        return multiplicacion;
    }

    function multMatrizStrassen(matrizU,matrizD){              
        if(matrizU.length==1){           
            return [[matrizU[0][0]*matrizD[0][0]]];
        }
        else{
            var M = new Array(7);
            var A = new Array(2);
            var B = new Array(2);
            var C = new Array(2);
            for (i=0;i<2;i++){
                A[i] = new Array(2);        
                B[i] = new Array(2);
                C[i] = new Array(2);        
                for (j=0;j<2;j++){
                    A[i][j] = new Array(matrizU.length/2);        
                    B[i][j] = new Array(matrizU.length/2);
                    C[i][j] = new Array(matrizU.length/2);
                    for (k=0;k<matrizU.length/2;k++){
                        A[i][j][k] = new Array(matrizU.length/2);        
                        B[i][j][k] = new Array(matrizU.length/2);
                        C[i][j][k] = new Array(matrizU.length/2);
                    }
                }        
            }
            for (i=0;i<2;i++){
                for (j=0;j<2;j++){
                    for (k=0;k<matrizU.length/2;k++){
                        for (l=0;l<matrizU.length/2;l++){
                            A[i][j][k][l] = matrizU[(i*matrizU.length/2)+k][(j*matrizU.length/2)+l];
                            B[i][j][k][l] = matrizD[(i*matrizU.length/2)+k][(j*matrizU.length/2)+l];                       
                        }               
                    }
                }        
            }
                   
            M[0] = multMatrizStrassen(sumar(A[0][0],A[1][1]),sumar(B[0][0],B[1][1]));
            M[1] = multMatrizStrassen(sumar(A[1][0],A[1][1]),B[0][0]);
            M[2] = multMatrizStrassen(A[0][0],restar(B[0][1],B[1][1]));
            M[3] = multMatrizStrassen(A[1][1],restar(B[1][0],B[0][0]));
            M[4] = multMatrizStrassen(sumar(A[0][0],A[0][1]),B[1][1]);
            M[5] = multMatrizStrassen(restar(A[1][0],A[0][0]),sumar(B[0][0],B[0][1]));
            M[6] = multMatrizStrassen(restar(A[0][1],A[1][1]),sumar(B[1][0],B[1][1]));
            contadorStrassen = contadorStrassen+7;
            C[0][0] = sumar(sumar(M[0],M[3]),restar(M[6],M[4]));            
            C[0][1] = sumar(M[2],M[4]);
            C[1][0] = sumar(M[1],M[3]);
            C[1][1] = sumar(restar(M[0],M[1]),sumar(M[2],M[5]));
            
            var resultado = new Array(matrizU.length);
            for (i=0;i<matrizU.length;i++){
                resultado[i] = new Array(matrizU[i].length);        
                for (j=0;j<resultado[i].length;j++){
                    resultado[i][j] = 0;
                }                
            }
            for (i=0;i<2;i++){
                for (j=0;j<2;j++){
                    for (k=0;k<matrizU.length/2;k++){
                        for (l=0;l<matrizU[k].length/2;l++){
                            resultado[(i*matrizU.length/2)+k][(j*matrizU.length/2)+l] = C[i][j][k][l];
                        }               
                    }
                }        
            }
            return resultado;
        }
    }

    function limpiar(identificador){
        var contenedor = document.getElementById(identificador);
        if (contenedor){
	    	contenedor.parentNode.removeChild(contenedor);
	    }
    }

    function crearMatrices(){
        crearMatriz("matrizP","MatrizA");
        crearMatriz("matrizS","MatrizB");
    }
    function crearMatriz(titleCont, idResult){
        limpiar(titleCont+"contenedor");
        var pot = parseInt(document.getElementById("tamanoMatriz").value);
        var tam = Math.pow(2,pot);
        var principal =  document.getElementById(titleCont);    
        var contenedor = document.createElement("div");
        contenedor.id = titleCont+"contenedor";
        principal.appendChild(contenedor);
        var elementos = new Array(tam);
        for (i=0;i<tam;i++){
            var fila = document.createElement("div");
            contenedor.appendChild(fila);        
            elementos[i] = new Array(tam);
            for (j=0;j<tam;j++){
                elementos[i][j] = document.createElement("input");
                elementos[i][j].type = "text";
                elementos[i][j].id = idResult+i.toString()+j.toString();
                elementos[i][j].size = "1";
                elementos[i][j].value = "0";
                fila.appendChild(elementos[i][j]);
            }
        }
    }
    function asignarValor(contenedor,nombreId,matriz){
        crearMatriz(contenedor,nombreId);
        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){
                var asignar = document.getElementById(nombreId+i.toString()+j.toString());
                asignar.value = matriz[i][j].toString();
            }        
        }
    }    
    function capturarMatrices(){        
        contadorStrassen = 0;
        var pot = parseInt(document.getElementById("tamanoMatriz").value);
        var tam = Math.pow(2,pot);
        var inter=0;
        var contador = 0;
        var matrizA = new Array(tam);
        for(var i=0;i<tam;i++){
            matrizA[i] = new Array(tam);
            for(var j=0;j<tam;j++){
                matrizA[i][j] = parseFloat(document.getElementById("MatrizA"+i.toString()+j.toString()).value);
            }    
        }

        var matrizB = new Array(tam);
        for(var i=0;i<tam;i++){
            matrizB[i] = new Array(tam);
            for(var j=0;j<tam;j++){
                matrizB[i][j] = parseFloat(document.getElementById("MatrizB"+i.toString()+j.toString()).value);
            }    
        }
            
        asignarValor("matrizR","MatrizR",multMatriz(matrizA,matrizB));
        asignarValor("matrizRS","MatrizRS",multMatrizStrassen(matrizA,matrizB));
        var imp = document.getElementById("strassen");
        imp.innerHTML=contadorStrassen.toString();
    }



